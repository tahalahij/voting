pragma solidity ^0.5.0;

  contract Voting {  

    struct Vote{
        address voterAddress;
        bool choice; // if true means voter thinks a violation has occurred otherwise false
    }
    
    struct Voter{
        string voterName;
        bool voted;  // if true, that person already voted
    }

    uint private currentResult = 0;
    uint public finalResult = 0; // what gets returned as result of voting
    uint public totalVoter = 0; // all voters participated
    uint public totalVote = 0; // all votes
    address public creatorsAddress;    
    string public creatorsName;
    
    
    mapping(uint => Vote) private votes;
    mapping(address => Voter) public voterRegister;
    
    enum State { Created, Voting, Ended }
    State public state;

     constructor ( string memory _creatorsName) public {
    creatorsAddress = msg.sender;
    creatorsName = _creatorsName;

 
    
    state = State.Created;
}    

modifier 
condition(bool _condition) {
require(_condition);
_;
}

modifier onlyOfficial() {
require(msg.sender == creatorsAddress);	
_;
}

modifier inState(State _state) {
require(state == _state);
_;
}

event voterAdded(address voter);
event voteStarted();
event voteEnded(uint finalResult);
event voteDone(address voter);

    //add voter
function addVoter(address _voterAddress, string memory _voterName) public inState(State.Created) onlyOfficial
{
    Voter memory v;
    v.voterName = _voterName;
    v.voted = false;
    voterRegister[_voterAddress] = v;
    totalVoter++;
    emit voterAdded(_voterAddress);
}

//declare voting starts now
function startVote() public inState(State.Created) onlyOfficial
{
    state = State.Voting;     
    emit voteStarted();
}

//voters vote by indicating their choice (true/false)
function doVote(bool _choice) public inState(State.Voting) returns (bool voted){
    bool found = false;
    
    if (bytes(voterRegister[msg.sender].voterName).length != 0 
    && !voterRegister[msg.sender].voted){
        voterRegister[msg.sender].voted = true;
        Vote memory v;
        v.voterAddress = msg.sender;
        v.choice = _choice;
        if (_choice){
            currentResult++; //counting on the go
        }
        votes[totalVote] = v;
        totalVote++;
        found = true;
    }
    emit voteDone(msg.sender);
    return found;
}

//end votes
function endVote() public inState(State.Voting) onlyOfficial {
        state = State.Ended;
        finalResult = currentResult; //move result from private currentResult to public finalResult
        emit voteEnded(finalResult);
    }
}